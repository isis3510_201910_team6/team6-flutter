import '../Connectivity/MessageHandler.dart';
import '../MemoryGame/Players.dart';
import '../MemoryGame/Player.dart';


class CommandManager {

  static void manageCommand(String message,Command command,Players players){
    print('in command manager');
    String content =message.split(':')[1];
    if (command==Command.PLAYER_USERNAME){
      String username =content;
      Player p = Player(username);
      players.remotePlayers.add(p);
    }
  }


  static String getCommandString(Command command){
    return command.toString().split('.')[1];
  }
}