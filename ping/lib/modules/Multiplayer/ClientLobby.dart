import 'package:flutter/material.dart';
import 'package:ping/modules/Connectivity/MessageHandler.dart';
import 'package:ping/modules/MemoryGame/Player.dart';
import 'package:ping/modules/MemoryGame/Players.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../Connectivity/ClientConnectivityManager.dart';
import './MultiplayerGameClient.dart';

class ClientLobby extends StatefulWidget {
  String ip;
  ClientLobby(String pIp) {
    ip = pIp;
  }
  @override
  State<StatefulWidget> createState() {
    return _ClientLobbyState(ip);
  }
}

class _ClientLobbyState extends State<ClientLobby> {
  String username;
  ClientConnectivityManager ccm;
  String _ip;
  Players players;
  bool couldConnect = false;

  _ClientLobbyState(String pIp) {
    _ip = pIp;

    init();
  }

  playersContain(String pUsername) {
    for (Player p in players.remotePlayers) {
      if (p.username == pUsername) {
        return true;
      }
    }
    return false;
  }

  init() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    setState(() {
      username = sp.get('username');
    });
    ClientConnectivityManager tempccm = ClientConnectivityManager((message) {
      Command command = MessageHandler.getCommand(message);
      // print(command.toString());
      if (command == Command.PLAYERS_USERNAMES) {
        List<String> incomingUsernames =
            MessageHandler.getInfo(message).split(';');

        for (String incomingUsername in incomingUsernames) {
          if (!playersContain(incomingUsername.trim())) {
            Player tempPlayer = Player(incomingUsername);
            setState(() {
              players.remotePlayers.add(tempPlayer);
            });
          }
        }
      } else if (command == Command.PLAYERS_STATUS) {
        // PLAYERS_STATUSS:<player_username>,<player_score>,<player_has_lost>;...
        List<String> infos = MessageHandler.getInfo(message).split(';');
        print(infos);

        for (String info in infos) {
          List<String> data = info.split(',');

          String tempUsername = data[0];
          int tempScore = int.parse(data[1]);
          bool tempHasLost = data[2].toLowerCase() == 'true';

          for (int i = 0; i < players.remotePlayers.length; i++) {
            Player p = players.remotePlayers[i];
            if (p.username == tempUsername) {
              setState(() {
                p.score = tempScore;
                p.isDead = tempHasLost;
                // players.remotePlayers.sort((a,b)=>b.score-a.score);
              });
            }
          }
        }
      } else if (command == Command.START_GAME) {
        print('game started!');
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => MultiplayerGameClient(ccm, players)));
      }

      print('number of players is: ${players.remotePlayers.length}');
    });

    String toSend = 'PLAYER_USERNAME:';
    toSend += username;
    couldConnect = await tempccm.connect(_ip, toSend);
    print("couldConnect");
    print(couldConnect);
    if(!couldConnect){
      print("Returning to create or join");
      Navigator.pop(context,false);
    }
    Player me = Player(username);
    Players tempPlayers = Players(me);

    setState(() {
      ccm = tempccm;
      players = tempPlayers;
    });
  }

  @override
  @mustCallSuper
  dispose() {
    ccm.closeConnection();
    super.dispose();
  }

  getRenderTexts() {
    List<String> ans = [
      'Waiting for party leader to start game.',
      'Connected players'
    ];

    if (players != null) {
      if (players.me.isDead) {
        ans = ['You Messed Up. Better luck next time!', 'Leaderboard'];
      }
    }

    return ans;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
        Container(
          child: Text(
            '$username',
            style: TextStyle(fontSize: 30),
          ),
          padding: EdgeInsets.only(top: 100, bottom: 20),
        ),
        Text(getRenderTexts()[0]),
        Container(
          child: Text(getRenderTexts()[1],
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
          padding: EdgeInsets.only(top: 30),
        ),
        _buildPlayers(context),
      ]),
    ));
  }

  checkAndSortPlayers() {
    if (players == null) {
      return 0;
    } else {
      players.remotePlayers.sort((a, b) => b.score - a.score);
      return players.remotePlayers.length;
    }
  }

  Widget _buildPlayers(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: checkAndSortPlayers(),
        itemBuilder: (BuildContext context, int index) {
          return makeCard(index);
        },
      ),
    );
  }

  Widget makeCard(int index) {
    return Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Container(
        decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
        child: makeListTile(index),
      ),
    );
  }

  Widget makeListTile(int index) {
    return ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 10.0),
        title: Container(
          padding: EdgeInsets.only(left: 20),
          child: Text(
            players.remotePlayers[index].username,
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
        trailing: Container(
          padding: EdgeInsets.only(right: 20),
          child: Text(
            players.remotePlayers[index].score.toString(),
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ));
  }
}
