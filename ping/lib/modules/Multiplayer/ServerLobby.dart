import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../Connectivity/ServerConnectivityManager.dart';
import '../MemoryGame/Players.dart';
import '../MemoryGame/Player.dart';
import '../Connectivity/MessageHandler.dart';
import './MultiplayerGame.dart';
import '../Multiplayer/CommandManager.dart';

class ServerLobby extends StatefulWidget {
  const ServerLobby({Key key}) : super(key: key);

  @override
  _ServerLobbyState createState() => _ServerLobbyState();
}

class _ServerLobbyState extends State<ServerLobby> {
  String _ip;
  ServerConnectivityManager scm;
  Players players;
  String username;

  _ServerLobbyState() {
    init();
  }

  init() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String tempIP = await ServerConnectivityManager.getIp();
    // String tempIP = '00000';

    ServerConnectivityManager tempscm = ServerConnectivityManager((message) {
      Command command = MessageHandler.getCommand(message);

      if (command == Command.PLAYER_USERNAME) {
        String incomingUsername = MessageHandler.getInfo(message);
        Player p = Player(incomingUsername);
        setState(() {
          players.remotePlayers.add(p);
        });

        String toBroadcast = 'PLAYERS_USERNAMES:';

        for (Player p in players.remotePlayers) {
          toBroadcast += p.username + ';';
        }
        toBroadcast =toBroadcast.substring(0,toBroadcast.length-1);
        // toBroadcast += players.me.username;
        // toBroadcast =toBroadcast
        scm.broadCastMessage(toBroadcast);
      } else if (command == Command.PLAYER_STATUS) {
        // PLAYER_STATUS:<player_username>,<player_score>,<player_has_lost>
        String info = MessageHandler.getInfo(message);
        print(info);
        List<String> data = info.split(',');

        String tempUsername = data[0];
        int tempScore = int.parse(data[1]);
        bool tempHasLost = data[2].toLowerCase() == 'true';

        for (int i = 0; i < players.remotePlayers.length; i++) {
          Player p = players.remotePlayers[i];
          if (p.username == tempUsername) {
            setState(() {
              p.score = tempScore;
              p.isDead = tempHasLost;
              // players.remotePlayers.sort((a,b)=>b.score-a.score);
            });
          }
        }
        String toBroadcast = "PLAYERS_STATUS:";
        for (Player p in players.remotePlayers) {
          toBroadcast += p.username +
              ',' +
              p.score.toString() +
              ',' +
              p.isDead.toString() +
              ';';
        }
        toBroadcast += players.me.username +
            ',' +
            players.me.score.toString() +
            ',' +
            players.me.isDead.toString();
        scm.broadCastMessage(toBroadcast);
      }

      print('number of players is: ${players.remotePlayers.length}');
    });

    tempscm.connect();
    setState(() {
      username = sp.get('username');
      _ip = tempIP;
      Player me = Player(username);
      players = Players(me);
      scm = tempscm;
      print(me.username);
      print(players.remotePlayers.length);
    });
  }

  startGame() {
    scm.broadCastMessage(CommandManager.getCommandString(Command.START_GAME));
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => MultiplayerGame(scm, players)));
  }

  @override
  @mustCallSuper
  dispose() {
    scm.closeConnections();
    super.dispose();
  }

  Widget startButton() {
    Widget ans = Container(
      width: 300,
      height: 70,
      padding: EdgeInsets.only(top: 10),
      child: RaisedButton(
        color: Colors.black,
        textColor: Colors.white,
        child: Text('Start Game', style: TextStyle(fontSize: 20)),
        onPressed: () {
          startGame();
        },
      ),
    );
    if(players!=null){
      if(players.me.isDead){
        ans =Text("Please wait for all players to finish the game before exiting");
      }
    }
    return ans;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
        Container(
          child: Text(
            '$username',
            // textAlign: TextAlign.center,
            style: TextStyle(fontSize: 30),
          ),
          padding: EdgeInsets.only(top: 100, bottom: 20),
        ),
        lobbyOrLost(),
        _buildPlayers(context),
        startButton(),
      ]),
    ));
  }

  Widget lobbyOrLost() {
    Widget ans = Column(children: [
      Text('Your IP is:'),
      Text('$_ip', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      Container(
        child: Text('Players connected',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
        padding: EdgeInsets.only(top: 30),
      )
    ]);
    if (players != null) {
      if (players.me.isDead) {
        ans = Column(
          children: [
            Text('You Messed Up. Better luck next time!'),
            Container(
              child: Text('Leaderboard',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
              padding: EdgeInsets.only(top: 30),
            )
          ],
        );
      }
    }

    return ans;
  }

  checkAndSortPlayers() {
    if (players == null) {
      return 0;
    } else {
      players.remotePlayers.sort((a, b) => b.score - a.score);
      return players.remotePlayers.length;
    }
  }

  Widget _buildPlayers(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: checkAndSortPlayers(),
        itemBuilder: (BuildContext context, int index) {
          return makeCard(index);
        },
      ),
    );
  }

  Widget makeCard(int index) {
    return Card(
      elevation: 8.0,
      margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
      child: Container(
        decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
        child: makeListTile(index),
      ),
    );
  }

  Widget makeListTile(int index) {
    return ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 10.0),
        title: Container(
          padding: EdgeInsets.only(left: 20),
          child: Text(
            players.remotePlayers[index].username,
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
        trailing: Container(
          padding: EdgeInsets.only(right: 20),
          child: Text(
            players.remotePlayers[index].score.toString(),
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ));
  }
}
