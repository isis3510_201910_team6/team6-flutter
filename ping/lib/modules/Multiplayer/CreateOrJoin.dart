import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import './ServerLobby.dart';
import './ClientLobby.dart';

class CreateOrJoin extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CreateOrJoin();
  }
}

class _CreateOrJoin extends State<CreateOrJoin> {
  double w = 300;
  double h = 200;
  final ipController = TextEditingController();
  bool _joinDisabled = true;

  bool _activeFlushbar = false;
  Flushbar _flush;

  bool _activeFlushbarConnect = false;
  Flushbar _flushConnect;

  _CreateOrJoin() {
    _flush = Flushbar<bool>(
        title: "Invalid IP",
        message: "Please check that the ip is written correctly.",
        onStatusChanged: manageFlushbar,
        mainButton: FlatButton(
          onPressed: () => _flush.dismiss(),
          child: Text(
            "OK",
            style: TextStyle(color: Colors.amber),
          ),
        ) //
        );
    _flushConnect = Flushbar<bool>(
        title: "Could not connect",
        message:
            "Could not connect to host: ${ipController.text}. Please check that you are on the same network.",
        onStatusChanged: manageFlushbarConnect,
        mainButton: FlatButton(
          onPressed: () => _flushConnect.dismiss(),
          child: Text(
            "OK",
            style: TextStyle(color: Colors.amber),
          ),
        ) //
        );
  }

  void manageFlushbar(FlushbarStatus status) {
    if (status == FlushbarStatus.SHOWING ||
        status == FlushbarStatus.IS_APPEARING) {
      _activeFlushbar = true;
    } else if (status == FlushbarStatus.DISMISSED ||
        status == FlushbarStatus.IS_HIDING) {
      _activeFlushbar = false;
    }
  }

  void manageFlushbarConnect(FlushbarStatus status) {
    if (status == FlushbarStatus.SHOWING ||
        status == FlushbarStatus.IS_APPEARING) {
      _activeFlushbarConnect = true;
    } else if (status == FlushbarStatus.DISMISSED ||
        status == FlushbarStatus.IS_HIDING) {
      _activeFlushbarConnect = false;
    }
  }

  isValidIp(String inIp) {
    List<String> splitIp = inIp.split(".");
    if (splitIp.length != 4) {
      return false;
    }
    for (String ipNum in splitIp) {
      int daNum;
      try {
        daNum = int.parse(ipNum);
      } catch (e) {
        return false;
      }

      if (daNum < 0 || daNum > 255) {
        return false;
      }
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(top: 150),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  width: w,
                  height: h,
                  // padding: EdgeInsets.only(top:100),
                  child: RaisedButton(
                    color: Colors.black,
                    textColor: Colors.white,
                    child:
                        Text('Create Session', style: TextStyle(fontSize: 30)),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ServerLobby()));
                    },
                  ),
                ),
                Container(
                  width: 300,
                  child: TextField(
                    maxLength: 4 * 3 + 3,
                    controller: ipController,
                    decoration: InputDecoration(labelText: 'Enter IP'),
                    keyboardType: TextInputType.number,
                    onChanged: (String textInput) {
                      if (textInput != "") {
                        setState(() {
                          _joinDisabled = false;
                        });
                      }
                    },
                  ),
                ),
                Container(
                  width: w,
                  height: h,
                  // padding: EdgeInsets.only(top:),
                  child: RaisedButton(
                    color: Colors.black,
                    textColor: Colors.white,
                    child: Text('Join', style: TextStyle(fontSize: 30)),
                    onPressed: _joinDisabled
                        ? null
                        : () async {
                            var couldConnect = false;
                            if (isValidIp(ipController.text)) {
                              couldConnect = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          ClientLobby(ipController.text)));

                              if (couldConnect != null) {
                                if (!couldConnect) {
                                  _flushConnect.show(context);
                                }
                              }
                            } else {
                              if (!_activeFlushbar) {
                                _flush.show(context);
                              }
                            }
                          },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
