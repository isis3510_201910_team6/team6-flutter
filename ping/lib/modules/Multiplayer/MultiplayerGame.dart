import 'dart:math';

import 'package:audioplayers/audio_cache.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:ping/modules/MemoryGame/MemoryGame.dart';
import 'package:ping/modules/MemoryGame/Player.dart';
import 'package:swipedetector/swipedetector.dart';
import 'package:vibrate/vibrate.dart';
import '../Connectivity/ServerConnectivityManager.dart';
import '../MemoryGame/Players.dart';

class MultiplayerGame extends StatefulWidget {
  //stuff for connectivity
  ServerConnectivityManager scm;
  Players players;

  MultiplayerGame(ServerConnectivityManager pScm, Players pPlayers) {
    scm = pScm;
    players = pPlayers;
  }

  @override
  State<StatefulWidget> createState() {
    return _MultiplayerGameState(scm, players);
  }
}

class _MultiplayerGameState extends State<MultiplayerGame> {
  ServerConnectivityManager _scm;
  Players _players;

  //stuff for game
  int _score;
  List<Actions> _actions = [];
  var rng = new Random();
  FlutterTts flutterTts = new FlutterTts();
  static AudioCache player = new AudioCache();
  String success = "sounds/success.mp3";
  String swipe = 'sounds/swipe.mp3';
  String gameOverSound = 'sounds/gameOver.wav';
  String tapSound = 'sounds/tap.wav';
  int _cont;
  final Map<Actions, String> actionsToString = {
    Actions.DOWN: 'Down ',
    Actions.UP: 'Up ',
    Actions.LEFT: 'Left ',
    Actions.RIGHT: 'Right ',
    Actions.TAP: 'Tap '
  };

  _MultiplayerGameState(ServerConnectivityManager pScm, Players pPlayers) {
    _scm = pScm;
    _players = pPlayers;

    _cont = 0;
    _actions.add(chooseRandomAction());
    saySequence();
  }

  String getMaxPlayersScore() {
    int maxScore = 0;
    for (Player p in _players.remotePlayers) {
      if (p.score > maxScore) {
        maxScore = p.score;
      }
    }
    return maxScore.toString();
  }

  void successFeedback() {
    player.play(success, volume: 0.8);
    Vibrate.feedback(FeedbackType.success);
    setState(() {
      // _score++;
      //dude got 1 right!!
      _players.me.score += 1;
    });

    sendStatus();
  }

  sendStatus() {
    // String mToSend = 'PLAYER_STATUS:';
    // mToSend += _players.me.username+',';
    // mToSend += _players.me.score.toString()+',';
    // mToSend +=_players.me.isDead.toString();
    // _ccm.sendMessage(mToSend);
  }
  Future _speak(word) async {
    flutterTts.setSpeechRate(0.6);
    await flutterTts.speak(word);
  }

  void manageInput(Actions playerAction) {
    if (playerAction == Actions.TAP) {
      player.play(tapSound);
    } else {
      player.play(swipe, volume: 3.0);
    }
    if (_actions[_cont] == playerAction) {
      _cont++;
      if (_cont == _actions.length) {
        successFeedback();
        Future.delayed(const Duration(milliseconds: 700), newSequence);
      }
    } else {
      //game over
      player.play(gameOverSound);
      Vibrate.feedback(FeedbackType.error);
      _speak('Game over');

      setState(() {
        _players.me.isDead = true;
        sendStatus();
      });
      Navigator.pop(context);
      // sendFireBaseData();
    }
  }

  void newSequence() {
    _cont = 0;
    _actions.add(chooseRandomAction());
    saySequence();
  }

  void saySequence() async {
    String sequence = "";
    for (int i = 0; i < _actions.length; i++) {
      String act = actionsToString[_actions[i]];
      sequence += act;
    }
    await _speak(sequence);
    // _startReaction = Timestamp.now().millisecondsSinceEpoch;
  }

  Actions chooseRandomAction() {
    int numActions = Actions.values.length - 1;
    return Actions.values[rng.nextInt(numActions)];
  }

  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SwipeDetector(
                    onSwipeLeft: () {
                      manageInput(Actions.LEFT);
                    },
                    onSwipeRight: () {
                      manageInput(Actions.RIGHT);
                    },
                    onSwipeUp: () {
                      manageInput(Actions.UP);
                    },
                    onSwipeDown: () {
                      manageInput(Actions.DOWN);
                    },
                    child: GestureDetector(
                        onTap: () {
                          manageInput(Actions.TAP);
                        },
                        child: FractionallySizedBox(
                            widthFactor: 1.0,
                            heightFactor: 1.0,
                            child: Container(
                              color: Colors.transparent,
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.only(top: 100),
                                      child:Text('Top score: '+getMaxPlayersScore(),
                                      )),
                                       Text(
                                        '${_players.me.score}',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 40),
                                       ),
                                      
                                  
                                ],
                              ),
                            )))));
  }

  @override
  @mustCallSuper
  dispose() {
    super.dispose();
  }
}
