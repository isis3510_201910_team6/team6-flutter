enum Command {
  PLAYER_USERNAME,
  PLAYERS_USERNAMES,
  START_GAME,
  PLAYER_STATUS,
  PLAYERS_STATUS
}

class MessageHandler {
  static Command getCommand(String message) {
    String prefix = message.split(":")[0];
    for (Command c in Command.values) {
      if (prefix == c.toString().split('.')[1]) {
        // print(c.toString().split('.')[1]);
        // print(prefix);
        return c;
      }
    }
    throw "No command found";
  }

  static String getInfo(String message) {
    return message.split(":")[1];
  }

  // static String getUsername(message){
  //   String username
  // }
}
