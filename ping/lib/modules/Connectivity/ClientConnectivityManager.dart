import 'dart:io';

class ClientConnectivityManager {
  //see http://jamesslocum.com/post/67566023889 for more info
  Socket _socket;
  Function _callback;
  String _ip;
  int port;

  ClientConnectivityManager(Function myCallback) {
    _callback = myCallback;
  }

  Future<bool> connect(String ip, String initialMessage) async {
    bool ans = false;
    print('attempting to connect');
    await Socket.connect(
      ip,
      8888,
      timeout: Duration(seconds: 3)
    ).then((socket) {
      print('Connected to: '
          '${socket.remoteAddress.address}:${socket.remotePort}');

      _socket = socket;
      _ip = socket.remoteAddress.address;
      port = _socket.remotePort;

      sendMessage(initialMessage);
      _socket.listen((data) {
        String message = decypherMessage(data);
        // print('i am in clientconnectManager, message is: $message');
        _callback(message);
      }, onError: errorHandler, onDone: finishedHandler);
      ans=true;
    }).catchError((onError) {
      print('Could not connect.');
      print(onError.toString());
      
    });
    return ans;
  }

  void sendMessage(String message) {
    try {
      _socket.write(message + '\n');
    } catch (e) {
      _socket.close().then((a) => _socket.destroy());
      print('lost connection to server, closing socket');
    }
  }

  void errorHandler(error) {
    print('$_ip:$port Error: $error');
    // removeClient(this);
    _socket.close();
  }

  void finishedHandler() {
    print('$_ip:$port Disconnected');
    // removeClient(this);
    _socket.close();
    // _socket.destroy();
  }

  void closeConnection() {
    _socket.close().then((a) => _socket.destroy());
    print('socket closed');
    // _socket.destroy();
  }

  String decypherMessage(List<int> data) {
    String message = new String.fromCharCodes(data).trim();
    return message;
  }
}
