import 'dart:io';

class Client {
  Socket _socket;
  String ip;
  int port;

  Client(Socket s, Function callback) {
    _socket = s;
    ip = _socket.remoteAddress.address;
    port = _socket.remotePort;

    _socket.listen((data) {
      messageHandler(data, callback);
    }, onError: errorHandler, onDone: finishedHandler);
  }

  void messageHandler(List<int> data, Function callback) {
    String message = new String.fromCharCodes(data).trim();
    callback(message);
  }

  void errorHandler(error) {
    print('$ip:$port Error: $error');
    // removeClient(this);
    _socket.close();
  }

  void finishedHandler() {
    print('$ip:$port Disconnected');
    // print(
    // '${_socket.remoteAddress.address}:${_socket.remotePort} Disconnected');
    // removeClient(this);
    _socket.close();
  }

  void write(String message) {
    _socket.write(message+'\n');
  }

  void close() {
    _socket.close().then((a) => _socket.destroy());
  }
}
