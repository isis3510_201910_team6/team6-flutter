import 'dart:io';
import './Client.dart';
import 'package:wifi/wifi.dart';

class ServerConnectivityManager {
  // ServerSocket _serverSocket;
  List<Client> clients = [];
  ServerSocket _serverSocket;
  Function _callback;

  ServerConnectivityManager(Function myCallback) {
    _callback = myCallback;
  }

  void connect() {
    ServerSocket.bind(InternetAddress.anyIPv4, 8888)
        .then((ServerSocket socket) {
      print('Server socket bound to device on port 8888.');
      _serverSocket = socket;

      _serverSocket.listen((client) {
        handleConnection(client);
      });

    });
  }

  handleConnection(Socket s) {
    print('Connection from '
        '${s.remoteAddress.address}:${s.remotePort}');

    clients.add(new Client(s, _callback));

  }

  void broadCastMessage(String message) {
    List<Client> toRemove = [];
    for (Client c in clients) {
      try {
        c.write(message);
      } catch (e) {
        print('couldnt send message, closing connection');
        c.close();
        toRemove.add(c);
      }      
    }

    for(Client c in toRemove){
      clients.remove(c);
    }
  }

  void closeConnections() {
    for (int i = 0 ; i<clients.length ; i++) {
      clients[i].close();
      clients.removeAt(0);
    }
    _serverSocket.close();
    print('closed connections.');
  }

  static Future<String> getIp() async {
    String tempIP = await Wifi.ip;
    return tempIP;
  }
}
