import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ping/main.dart' as prefix0;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flushbar/flushbar.dart';
// import 'dart:io';
import './Utils/NetworkCheck.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:connectivity/connectivity.dart';
import './MemoryGame/MemoryGameMenu.dart';

class RegisterUser extends StatefulWidget {
  _RegisterUserState createState() => _RegisterUserState();
}

class _RegisterUserState extends State<RegisterUser> {
  String _username;
  bool _isRegistering = false;
  bool _activeFlushEmptyName = false;
  bool _activeFlushInternet = false;
  bool _activeFlushTakenName = false;
  Flushbar _flushTakenName;
  Flushbar _flushEmptyName;
  Flushbar _flushInternet;
  SharedPreferences sp;
  // bool _connected;
  _RegisterUserState() {
    username();
    _flushEmptyName = Flushbar<bool>(
                  title:  "Username cannot be empty",
                  message:  "Please, enter a valid username.",
                  onStatusChanged: manageFlushEmptyName,
                  mainButton: FlatButton(
                                onPressed: () => _flushEmptyName.dismiss(),
                                child: Text(
                                  "OK",
                                  style: TextStyle(color: Colors.amber),
                                ),
                              ) //            
                 );

    _flushTakenName = Flushbar<bool>(
                  title:  "Username is already taken",
                  message:  "Please, enter a valid username.",
                  onStatusChanged: manageFlushTakenName,
                  mainButton: FlatButton(
                                onPressed: () => _flushTakenName.dismiss(),
                                child: Text(
                                  "OK",
                                  style: TextStyle(color: Colors.amber),
                                ),
                              ) //            
                 );

    _flushInternet = Flushbar<bool>(
                      title:  "No internet connection",
                      message:  "Cannot register username.",
                      onStatusChanged: manageFlushInternet,
                      mainButton: FlatButton(
                                    onPressed: () => _flushInternet.dismiss(),
                                    child: Text(
                                      "OK",
                                      style: TextStyle(color: Colors.amber),
                                    ),
                                  ) //            
                     );
    
  }

  void manageFlushEmptyName(FlushbarStatus status) {
    if (status == FlushbarStatus.SHOWING || status == FlushbarStatus.IS_APPEARING) {
      _activeFlushEmptyName = true;
    } 
    else if (status == FlushbarStatus.DISMISSED || status == FlushbarStatus.IS_HIDING) {
      _activeFlushEmptyName = false;
    }
  }

  void manageFlushTakenName(FlushbarStatus status) {
    if (status == FlushbarStatus.SHOWING || status == FlushbarStatus.IS_APPEARING) {
      _activeFlushTakenName = true;
    } 
    else if (status == FlushbarStatus.DISMISSED || status == FlushbarStatus.IS_HIDING) {
      _activeFlushTakenName = false;
    }
  }


  void manageFlushInternet(FlushbarStatus status) {
    if (status == FlushbarStatus.SHOWING || status == FlushbarStatus.IS_APPEARING) {
      _activeFlushInternet = true;
    } 
    else if (status == FlushbarStatus.DISMISSED || status == FlushbarStatus.IS_HIDING) {
      _activeFlushInternet = false;
    }
  }

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
  }
  
  TextEditingController _usernameController = TextEditingController();


  Widget userLogin() {
    return Scaffold(
        backgroundColor: prefix0.backgroundColor,
        resizeToAvoidBottomPadding: false,
        body: Center(
            child: Column(
          children: <Widget>[
            Padding(
              child: Text(
                'Sign Up',
                style: TextStyle(fontSize: 80, color: prefix0.elementColor),
              ),
              padding: EdgeInsets.only(top: 100, bottom: 40),
            ),
            Container(
                width: 300,
                child: Center(
                    child: Text(
                        'This will allow you to access the ranking and compete with other players',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 20, color: prefix0.elementColor)))),
            Padding(
                padding: EdgeInsets.only(top: 10, bottom: 20),
                child: Container(
                    width: 300,
                    child: Semantics(
                      hint: "Enter username maximum 15 characters",
                      child: TextField(
                      controller: _usernameController,
                      cursorColor: prefix0.backgroundColor,
                      maxLength: 15,
                      decoration: InputDecoration(hintText: 'username', fillColor: prefix0.elementColor, filled: true),

                    )))),
            Padding(
              child: Container(
                  width: 300,
                  child: RaisedButton(
                    color: prefix0.buttonColor,
                    textColor: prefix0.elementColor,
                    child: Text('SIGN-UP', style: TextStyle(fontSize: 15, color: prefix0.elementColor)),
                    onPressed: _isRegistering ? null : () {
                      NetworkCheck networkCheck = new NetworkCheck();
                      networkCheck.checkInternet(handleRegister);
                    },
                  )),
              padding: EdgeInsets.only(top: 10),
            ),
            Padding(
              child: Container(
                  width: 300,
                  child: RaisedButton(
                    color: prefix0.buttonColor,
                    textColor: prefix0.elementColor,
                    child: Text('NO THANKS, MAYBE LATER',
                        style: TextStyle(fontSize: 15)),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MemoryGameMenu()));
                    },
                  )),
              padding: EdgeInsets.only(top: 10),
            )
          ],
        )));
  }

  Future<String> registerUser(String username) async {
    // Firestore.instance.enablePersistence(true);
    //_usernameController.
    //if (_isRegistering) return null;
    DocumentReference ref;
    String documentID;
    QuerySnapshot users = await Firestore.instance
        .collection('Players').where('username',isEqualTo: username).getDocuments();
    if (users.documents.isEmpty) {
      Map<String, dynamic> data = {'highscore': 0, 'username': username};
      ref = await Firestore.instance.collection('Players').add(data);
      documentID = ref.documentID;
    }
    
    print('created $documentID with username $username');
    
    return documentID;
  }

  void handleRegister(bool isNetworkPresent) async {
    String inputUsername = _usernameController.text.trim();
      if ( inputUsername != '') {

        if (isNetworkPresent) {
          setState( () => _isRegistering = true);
          String documentID = await registerUser(inputUsername);
          setState( () => _isRegistering = false);
          if (documentID != null) {
            await sp.setString('username', inputUsername);
            await sp.setString('user_id', documentID);
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => MemoryGameMenu()));
          }
          else if (!_activeFlushTakenName) {
            _flushTakenName.show(context);
          }
          
        } 
        else if (!_activeFlushInternet) {
          _flushInternet.show(context);   
        }
      } else if (!_activeFlushEmptyName) {
        _flushEmptyName.show(context);
      }
  }

  void username() async {
    sp = await SharedPreferences.getInstance();
    setState(() {
      _username = sp.get('username');
    });
  }

  Widget _buildChild() {
    // var username = await getUsername();
    if (_username != null) {
      print('username is not null, its $username');
      return MemoryGameMenu();
    } else {
      return userLogin();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(child: _buildChild());
  }
}
