import 'package:flutter/material.dart';
import 'dart:math';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:ping/main.dart';
import 'package:swipedetector/swipedetector.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'dart:async';
import 'package:vibrate/vibrate.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../Utils/NetworkCheck.dart';
import 'package:shake/shake.dart';

// import 'package:firebase_analytics/observer.dart';

final analytics = FirebaseAnalytics();

// final observer = FirebaseAnalyticsObserver(analytics: analytics);
class MemoryGame extends StatefulWidget {
  int _highscore;

  MemoryGame(this._highscore);
  @override
  _MemoryGameState createState() => _MemoryGameState(_highscore);
}

enum Actions { UP, RIGHT, DOWN, LEFT, TAP, SHAKE }

class _MemoryGameState extends State<MemoryGame>
    with TickerProviderStateMixin { 

  SharedPreferences sp;
  final String backgroundTrack = 'tetris.mp3';
  int _score;
  List<Actions> _actions = [];
  List<Actions> _possibleActions;
  var rng = new Random();
  FlutterTts flutterTts = new FlutterTts();
  AudioPlayer backgroundPlayer;
  static AudioCache player = new AudioCache(prefix: 'sounds/');
  String successSound = "success.mp3";
  String gameOverSound = 'gameOver.wav';
  Timestamp startTimestamp = Timestamp.now();
  int _highscore;
  List<int> _reactionTime;
  int _startReaction;
  int _cont;
  final Map<Actions, String> actionsToString = {
    Actions.DOWN: 'Down ',
    Actions.UP: 'Up ',
    Actions.LEFT: 'Left ',
    Actions.RIGHT: 'Right ',
    Actions.TAP: 'Tap ',
    Actions.SHAKE: 'Shake '
  };
  AnimationController _controller;
  AnimationController _controllerText;
  Animation _animation;
  Animation _animationText;
  ShakeDetector shake;
  double _opacityLevelAnimation = 0;
  int _verticalDirection = 1;
  int _horizontalDirection = 1;
  int _shakeValue = 0;
  bool _listenInput = false;
  bool _isStart = true;
  Color _animationColor = Colors.white; 
  Map<Actions, String> actionsToSound;
  final Color _colorText = Color(0xFFCA9995);
  final Color _colorBackgroundDark = Color(0xFF1C1F33);
  final Color _colorUp = Color(0xFFFF7670);
  final Color _colorRight = Color(0xFFF3EE09);
  final Color _colorDown = Color(0xFFCF868F);
  final Color _colorLeft = Color(0xFFE5CACD);
  final Color _colorTap = Color(0xFF5D8B68);
  final Color _colorShake = Color(0xFF6382AB);


  _MemoryGameState(highscore) {
    _highscore = highscore;
    _score = 0;
    _cont = 0;
    _reactionTime = [];
    _startReaction = 0;
    _possibleActions = Actions.values.sublist(0, settings['controls']);
    setActionSounds();
    player.loadAll(actionsToSound.values.toList());
    if (settings['backgroundTrack']) {
      player.load(backgroundTrack);
      playBackgroundTrack();
    }
  }


  @override
  void initState() {
    super.initState();
    loadSharedPreferences();
    flutterTts.setSpeechRate(0.7);
    //flutterTts.setStartHandler(() => setState( () => _listenInput = false));
    shake = ShakeDetector.autoStart(
      onPhoneShake: () {
        manageInput(Actions.SHAKE);
      },
      shakeThresholdGravity: 2.7
    );
    _controller = AnimationController(duration: Duration(milliseconds: 400), vsync: this);
    _animation = Tween<double>(begin: 0, end: 1)
        .animate(_controller); //CurvedAnimation(parent: _controller, curve: Curves.linear)
    _animation.addStatusListener(changeOpacityLevel);

    _controllerText = AnimationController(duration: Duration(milliseconds: 500), vsync: this);
    _animationText = Tween<double>(begin: 0, end: 1).animate(_controllerText);
    _animationText.addStatusListener(fadeText);

    _actions.add(chooseRandomAction());
    
    Future.delayed(Duration(seconds: 1), () => saySequence());

    _controllerText.forward();
    flutterTts.speak('Ready. ').whenComplete(() => _isStart = false);

  }


  void loadSharedPreferences() async {
    sp = await SharedPreferences.getInstance();
  }

  @override
  void dispose() {
    _controller.dispose();
    shake.stopListening();
    if (settings['backgroundTrack']) stopBackgroundTrack();
    super.dispose();
  }

  void playBackgroundTrack() async {
     backgroundPlayer = await player.loop(backgroundTrack, volume: 0.8);
  }

  void stopBackgroundTrack() async {
    await backgroundPlayer.stop();
  }

  void fadeText(AnimationStatus status) {
    if (status == AnimationStatus.completed) {
      _controllerText.reverse();
    }

  }


  void changeOpacityLevel(AnimationStatus status) {
    if (status == AnimationStatus.forward) {
              setState(() {
                _opacityLevelAnimation = 1;
              });
    } else if (status == AnimationStatus.completed) {
              setState(() {
                _opacityLevelAnimation = 0;
                _controller.reset();
              });
    }
  }

 
  void sendFireBaseData({@required bool userExit}) async {

    String userId = sp.get('user_id');
    print('userID $userId');
    bool connection = await NetworkCheck().check();

    //Firestore.instance.enablePersistence(true);
    int duration = Timestamp.now().seconds - startTimestamp.seconds;
    List<int> sequence = _actions.map((action) {
      return Actions.values.indexOf(action);
    }).toList();
    double averageReactionTime = _reactionTime.length > 0 ?
        _reactionTime.reduce((value, element) => value + element) /
            _reactionTime.length : 0;
    // print('highscore $highscore');
    if (_score > _highscore) {
      _highscore = _score;
      // print('entra al if');
      await sp.setInt('highscore', _score);
      DocumentReference ref =
          Firestore.instance.collection('Players').document(userId);
      if (connection) {
        Firestore.instance.runTransaction((transaction) async {
          await transaction.update(ref, {'highscore': _score});
        });
      }
    }
    // Parametros  a enviar de analytics
    Map<String, dynamic> toSend = {
      'duration': duration,
      'score': _score,
      'highscore': _highscore,
      'timestamp': startTimestamp.seconds,
      'averageTimeBetweenReactions': averageReactionTime,
    };
    analytics.logEvent(name: 'game_finished', parameters: toSend);
    // Se agrega el sequence y se quita el highscore y el averageTimeBetweenReactions a toSend para guardar los mismos campos en la base de datos
    // Se cambia el timestamp de segundos a tipo Timestamp
    toSend['sequence'] = sequence;
    toSend['timestamp'] = startTimestamp;
    toSend.remove('highscore');
    toSend.remove('averageTimeBetweenReactions');
    if (connection) {
      await Firestore.instance
          .collection('Players')
          .document(userId)
          .collection('games')
          .add(toSend);
    }

    if (userExit) {
      Navigator.pop(context, true);
    } else {
      Navigator.pop(context, _highscore);
    }


    // Navigator.pop(context);

    // Firestore.instance.runTransaction((transaction) async {
    //     await transaction.set(ref,
    //     );
    // });
  }

  // @override
  // dispose(){
  //   super.dispose();
  //   Navigator.pop(context, _highscore);
  // }

  TickerFuture manageAnimation(Actions playerAction) {
    //final width = MediaQuery.of(context).size.width;
    //final height = MediaQuery.of(context).size.height;
    TickerFuture ticker;
    switch (playerAction) {
      case Actions.UP:
        _animationColor = _colorUp;
        _verticalDirection = -1;
        _horizontalDirection = 0;
        _shakeValue = 0;
        ticker = _controller.forward();
        break;
      case Actions.DOWN:
        _animationColor = _colorDown;
        _verticalDirection = 1;
        _horizontalDirection = 0;
        _shakeValue = 0;
        ticker = _controller.forward();
        break;
      case Actions.LEFT:
        _animationColor = _colorLeft;
        _horizontalDirection = -1;
        _verticalDirection = 0;
        _shakeValue = 0;
        ticker = _controller.forward();
        break;
      case Actions.RIGHT:
        _animationColor = _colorRight;
        _horizontalDirection = 1;
        _verticalDirection = 0;
        _shakeValue = 0;
        ticker = _controller.forward();
        break;
      case Actions.TAP:
        _animationColor = _colorTap;
        _horizontalDirection = 0;
        _verticalDirection = 0;
        _shakeValue = 0;
        ticker = _controller.forward();
        break;
      case Actions.SHAKE:
        _animationColor = _colorShake;
        _horizontalDirection = 0;
        _verticalDirection = 0;
        _shakeValue = 1;
        ticker = _controller.forward();
        break;
    }
    return ticker;
  }

  void animateSequence(int pos) {
    //if (pos >= _actions.length) return;
    manageAnimation(_actions[pos]).whenComplete(() {
      if (pos == _actions.length-1) {
        setState(() {
          _listenInput = true;
        });
        _controllerText.forward();
        //Future.delayed(Duration(seconds: 1), () => setState(() => _opacityLevelText = 0));
      }
      else animateSequence(pos + 1);
    });
  }

  void manageInput(Actions playerAction) {
    if(!_listenInput) return;
    _reactionTime.add(Timestamp.now().millisecondsSinceEpoch - _startReaction);
    _startReaction = Timestamp.now().millisecondsSinceEpoch;
    player.play(actionsToSound[playerAction]);
    manageAnimation(playerAction);
    if (_actions[_cont] == playerAction) {
      _cont++;
      if (_cont == _actions.length) {
        successFeedback();
        Future.delayed(const Duration(milliseconds: 700), newSequence);
      }
    } else {
      //game over
      _listenInput = false;
      player.play(gameOverSound);
      if (settings['negativeFeedback']) Vibrate.feedback(FeedbackType.error);
      flutterTts.speak('Game over');
      sendFireBaseData(userExit: false);
      //Navigator.pop(context, _highscore);
    }
    
  }

  void setActionSounds() {
    String soundType = settings['soundType'];
    switch (soundType) {
      case soundConga:
        actionsToSound = {
          Actions.RIGHT : 'conga/conga_right.wav',
          Actions.LEFT : 'conga/conga_left.wav',
          Actions.UP : 'conga/conga_up.wav',
          Actions.DOWN : 'conga/conga_down.wav',
          Actions.TAP : 'conga/conga_tap.wav',
          Actions.SHAKE : 'conga/conga_shake.wav'
        };
        break;
      case soundPiano:
        actionsToSound = {
          Actions.RIGHT : 'piano/piano_right.mp3',
          Actions.LEFT : 'piano/piano_left.mp3',
          Actions.UP : 'piano/piano_up.mp3',
          Actions.DOWN : 'piano/piano_down.mp3',
          Actions.TAP : 'piano/piano_tap.mp3',
          Actions.SHAKE : 'piano/piano_shake.mp3'
        };
        break;
      case soundNone:
        String none = '';
        actionsToSound = {
          Actions.RIGHT : none,
          Actions.LEFT : none,
          Actions.UP : none,
          Actions.DOWN : none,
          Actions.TAP : none,
          Actions.SHAKE : none
        };
        break;
      default:
        String swipeSound = 'default/default_swipe.mp3';
        actionsToSound = {
          Actions.RIGHT : swipeSound,
          Actions.LEFT : swipeSound,
          Actions.UP : swipeSound,
          Actions.DOWN : swipeSound,
          Actions.TAP : 'default/default_tap.wav',
          Actions.SHAKE : 'default/default_shake.wav'
        };
    }
  }

  void successFeedback() {
    if (settings['positiveFeedback']) player.play(successSound, volume: 0.8);
    Vibrate.feedback(FeedbackType.success);
    setState(() {
      _listenInput = false;
      _score++;
    });
  }

  void newSequence() {
    _cont = 0;
    _actions.add(chooseRandomAction());  
    saySequence();
  }

  void saySequence()  async {
    
    String sequence = '';
    for (Actions action in _actions) {
      sequence += actionsToString[action];
    }
    sequence += '. Your turn ';
    //setState(() => _opacityLevelText = 0);
    await flutterTts.speak(sequence);
    

    Future.delayed(Duration(milliseconds: 500), () => animateSequence(0));
    _startReaction = Timestamp.now().millisecondsSinceEpoch;
  }

  Actions chooseRandomAction() {
    int numActions = _possibleActions.length;
    return _possibleActions[rng.nextInt(numActions)];
  }


  Future<bool> _onWillPop(BuildContext context) {
    print('entra a onwillpop');

    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
                title: new Text('Are you sure?'),
                content: new Text('Do you want to exit the game?'),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () => Navigator.of(context).pop(false),
                    child: new Text('No'),
                  ),
                  new FlatButton(
                    onPressed: () {
                        //Navigator.of(context).pop(true);
                      sendFireBaseData(userExit: true);
                      },
                    child: new Text('Yes'),
                  ),
                ],
              ),
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return new WillPopScope(
        onWillPop: () => _onWillPop(context),
        child: new MaterialApp(
            title: 'Ping',
            home: Scaffold(
                backgroundColor: _listenInput ? Colors.white : _colorBackgroundDark,
                body: SwipeDetector(
                    onSwipeLeft: () {
                      manageInput(Actions.LEFT);
                    },
                    onSwipeRight: () {
                      manageInput(Actions.RIGHT);
                    },
                    onSwipeUp: () {
                      manageInput(Actions.UP);
                    },
                    onSwipeDown: () {
                      manageInput(Actions.DOWN);
                    },
                    child: GestureDetector(
                        onTap: () {
                          manageInput(Actions.TAP);
                        },
                        child: FractionallySizedBox(
                            widthFactor: 1.0,
                            heightFactor: 1.0,
                            child: Container(
                              color: Colors.transparent,
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                      padding: EdgeInsets.only(top: 100),
                                      child: Text(
                                        '$_score',
                                        style: TextStyle(
                                            color: _listenInput  ? Colors.black : _colorText,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 40),
                                      )),
                                  Stack(
                                    alignment: AlignmentDirectional.center,
                                    children: <Widget>[

                                      Padding(
                                          padding: EdgeInsets.only(top: 130),
                                          child: AnimatedBuilder(
                                              animation: _controller,
                                              builder: (BuildContext context,
                                                  Widget child) {
                                                return Opacity(
                                                    opacity: _opacityLevelAnimation,
                                                    child: Transform(
                                                        transform: Matrix4.translationValues(_animation.value * 150 * _horizontalDirection, _animation.value * 150 * _verticalDirection, 0),
                                                        child: Container(
                                                          width: _shakeValue * _animation.value * 20 + width / 10,
                                                          height: _shakeValue * _animation.value * 20 + height / 10,
                                                          decoration: BoxDecoration(
                                                              color: _animationColor,
                                                              shape: BoxShape.circle
                                                          ),
                                                        )));
                                              })),
                                      Container(
                                        child:  Padding(
                                            padding: EdgeInsets.only(top: 130),
                                            child: FadeTransition(
                                              opacity: _animationText,
                                              child: Text(
                                                _isStart ? 'READY?' : 'YOUR TURN!',
                                                style: TextStyle(
                                                  color: _colorText,
                                                  fontSize: 36,
                                                  fontWeight: FontWeight.w300
                                                ),
                                              ),
                                            ),
                                        ),
                                      )

                                    ],
                                  ),


                                ],
                              ),
                            )))))));
  }
}
