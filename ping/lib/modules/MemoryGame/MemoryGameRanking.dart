import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import '../Utils/NetworkCheck.dart';
import 'package:flushbar/flushbar.dart';
import 'package:ping/main.dart';

class MemoryGameRanking extends StatelessWidget {
  bool _activeFlushbar = false;
  Flushbar _flush;

  MemoryGameRanking() {
    _flush = Flushbar<bool>(
              title:  "No internet connection",
              message:  "Ranking may be missing or not updated.",
              onStatusChanged: manageFlushbar,
              mainButton: FlatButton(
                             onPressed: () => _flush.dismiss(),
                             child: Text(
                               "OK",
                               style: TextStyle(color: Colors.amber),
                             ),
                           ) //            
             );
  }

  @override
  Widget build(BuildContext context) {
    eventualConnectivity(context);
    return Scaffold(
      backgroundColor: backgroundColor,
      // appBar: AppBar(title: Text('Ranking')),
      body: Column(children: [
        Padding(
            padding: EdgeInsets.only(top: 50, bottom: 10),
            child: Text('Top 100',
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: elementColor))),
        Flexible(child: _buildBody(context)),
      ]),
    );
    // NetworkCheck nc =NetworkCheck();
  }

  void eventualConnectivity(BuildContext context) async {
    NetworkCheck nc = NetworkCheck();
    bool connected = await nc.check();
    if (!connected && !_activeFlushbar) {
       _flush.show(context);
    }
  }

  void manageFlushbar(FlushbarStatus status) {
    if (status == FlushbarStatus.SHOWING || status == FlushbarStatus.IS_APPEARING) {
      _activeFlushbar = true;
    } 
    else if (status == FlushbarStatus.DISMISSED || status == FlushbarStatus.IS_HIDING) {
      _activeFlushbar = false;
    }
  }

  Widget _buildBody(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance
          .collection('Players')
          .orderBy('highscore', descending: true)
          .limit(100)
          .snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) return LinearProgressIndicator();
        List<DocumentSnapshot> sortedList = snapshot.data.documents;
        sortedList.sort((doc1, doc2) {
          return (doc2.data['highscore'] - doc1.data['highscore']);
        });
        return _buildList(context, sortedList);
      },
    );
  }

  Widget _buildList(BuildContext context, List<DocumentSnapshot> snapshot) {
    return ListView(
      padding: const EdgeInsets.only(top: 20.0),
      children: snapshot.map((doc) {
        // print(doc.data.toString());

        if (doc != null) {
          return _buildListItem(context, doc);
        }
      }).toList(),
    );
  }

  Widget _buildListItem(BuildContext context, DocumentSnapshot doc) {
    return Padding(
      key: ValueKey(doc.documentID),
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 2.0),
      child: Container(
        width: 10,
        // color: Colors.red,
        // padding: ,
        decoration: BoxDecoration(
          color: backgroundColor,
          // boxShadow: ,
          // border: Border.all(color: Colors.grey),
          border: Border(
            // top:    BorderSide(width: 1.0, color: Colors.lightBlue.shade50),
            bottom: BorderSide(width: 1.0, color: elementColor),
          ),
          // borderRadius: BorderRadius.circular(5.0),
        ),
        child: ListTile(
            contentPadding: EdgeInsets.symmetric(horizontal: 80),
            title: Text(doc.data['username'] != ''
                ? doc.data['username']
                : 'Not registered', style: TextStyle(color: elementColor),),
            trailing: Text(
              doc.data['highscore'].toString(),
              style: TextStyle(color: elementColor),
            )),
      ),
    );
  }
}
