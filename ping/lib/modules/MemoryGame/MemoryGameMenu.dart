import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import './MemoryGame.dart';
import './MemoryGameRanking.dart';
import './MemoryGameSettings.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../Utils/NetworkCheck.dart';
// import '../Multiplayer/ServerLobby.dart';
import '../Multiplayer/CreateOrJoin.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:ping/main.dart';


class MemoryGameMenu extends StatefulWidget {
  @override
  _MemoryGameMenu createState() => _MemoryGameMenu();
}

class _MemoryGameMenu extends State<MemoryGameMenu> {
  int _highscore;
  String _username;
  Flushbar _flush;
  bool _showTutorial = false;
  bool _activeFlushbar = false;
  SharedPreferences sp;
  //final Color backgroundColor = Colors.white;
  //final  elementColor = Colors.black;

  _MemoryGameMenu() {
    print('i am the constructor');
    _highscore = 0;
    _username = 'Not Registered';
    checkAndGetHighscore();
    _flush = Flushbar<bool>(
        title: "User is not registered",
        message: "Cannot access multiplayer if user is not registered.",
        onStatusChanged: manageFlushbar,
        mainButton: FlatButton(
          onPressed: () => _flush.dismiss(),
          child: Text(
            "OK",
            style: TextStyle(color: Colors.amber),
          ),
        ) //
        );
  }

  void manageFlushbar(FlushbarStatus status) {
    if (status == FlushbarStatus.SHOWING ||
        status == FlushbarStatus.IS_APPEARING) {
      _activeFlushbar = true;
    } else if (status == FlushbarStatus.DISMISSED ||
        status == FlushbarStatus.IS_HIDING) {
      _activeFlushbar = false;
    }
  }

  void checkAndGetHighscore() async {
    sp = await SharedPreferences.getInstance();
    bool hasPlayed = sp.get('has_played') ?? false;
    if (hasPlayed) {
      _showTutorial = false;
    } else {
      _showTutorial = true;
    }
    DocumentSnapshot snap;
    DocumentReference ref =
        Firestore.instance.collection('Players').document(sp.get('user_id'));
    NetworkCheck nc = NetworkCheck();
    bool connected = await nc.check();

    setState(() {
      print('i am changing the state to 0');
      _highscore = sp.get('highscore') ?? 0;
      _username = sp.get('username') ?? 'Not Registered';
    });
    if (ref != null && connected) {
      await Firestore.instance.runTransaction((transaction) async {
        await transaction.update(ref, {'highscore': _highscore});
      });
    }
  }

  void updateHighscore() async {
    int result = await Navigator.push(context,
        MaterialPageRoute(builder: (context) => MemoryGame(_highscore)));
    if (result != null) {
      setState(() {
        print('i am changing the state to highscore $result');
        _highscore = result;
      });
    } else {
      setState( () {
        _highscore = sp.get('highscore') ?? _highscore;
      });
    }
  }

  void changeShowTutorial() {
    setState(() {
      _showTutorial = false;
    });
    sp.setBool('has_played', true);
  }

  void accessMultiplayer() {
    if (_username == 'Not Registered') {
      if (!_activeFlushbar) _flush.show(context);
    } else {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => CreateOrJoin()));
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_showTutorial) {
      return Scaffold(
          body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Swiper(
            itemBuilder: (BuildContext context, int index) {
              Widget page;
              switch (index) {
                case 0:
                  page = Container(
                    color: backgroundColor,
                    child: Center(
                      child: SingleChildScrollView(
                        child: Column(
                          children: <Widget>[
                            Padding(
                              child: Text(
                                'Welcome to Ping',
                                style: TextStyle(
                                  color: elementColor,
                                  fontSize: 40,
                                ),
                                textAlign: TextAlign.center,
                              ),
                              padding: EdgeInsets.all(25),
                            ),
                            Padding(
                              child: Text('A blind-firendly Simon says',
                                  style:
                                  TextStyle(color: elementColor, fontSize: 30),
                                  textAlign: TextAlign.center),
                              padding: EdgeInsets.all(25),
                            ),
                            Padding(
                              child: Text(
                                  'Listen to the instructions, remember them, repeat them and level up with one more instruction!',
                                  style:
                                  TextStyle(color: elementColor, fontSize: 20),
                                  textAlign: TextAlign.center),
                              padding: EdgeInsets.all(25),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                  break;
                case 1:
                  page = Container(
                    color: backgroundColor,
                    child: Center(
                      child: SingleChildScrollView(
                        child: Column(
                          children: <Widget>[
                            Padding(
                              child: Text(
                                'Possible instructions',
                                style: TextStyle(
                                  color: elementColor,
                                  fontSize: 30,
                                ),
                                textAlign: TextAlign.center,
                              ),
                              padding: EdgeInsets.all(50),
                            ),
                            Padding(
                              child: Text('Swipe up!\n(Voice command: Up)',
                                  style:
                                  TextStyle(color: elementColor, fontSize: 18),
                                  textAlign: TextAlign.center),
                              padding: EdgeInsets.all(10),
                            ),
                            Padding(
                              child: Text('Swipe down!\n(Voice command: Down)',
                                  style:
                                  TextStyle(color: elementColor, fontSize: 18),
                                  textAlign: TextAlign.center),
                              padding: EdgeInsets.all(10),
                            ),
                            Padding(
                              child: Text('Swipe left!\n(Voice command: Left)',
                                  style:
                                  TextStyle(color: elementColor, fontSize: 18),
                                  textAlign: TextAlign.center),
                              padding: EdgeInsets.all(10),
                            ),
                            Padding(
                              child: Text('Swipe right!\n(Voice command: Right)',
                                  style:
                                  TextStyle(color: elementColor, fontSize: 18),
                                  textAlign: TextAlign.center),
                              padding: EdgeInsets.all(10),
                            ),
                            Padding(
                              child: Text('Tap screen!\n(Voice command: Tap)',
                                  style:
                                  TextStyle(color: elementColor, fontSize: 18),
                                  textAlign: TextAlign.center),
                              padding: EdgeInsets.all(10),
                            ),
                            Padding(
                              child: Text('Shake phone!\n(Voice command: Shake)',
                                  style:
                                  TextStyle(color: elementColor, fontSize: 18),
                                  textAlign: TextAlign.center),
                              padding: EdgeInsets.all(10),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                  break;
                case 2:
                  page = Container(
                    color: backgroundColor,
                    child: Center(
                      child: SingleChildScrollView(
                        child: Column(
                          children: <Widget>[
                            Padding(
                              child: Text(
                                'Remember',
                                style: TextStyle(
                                    color: elementColor,
                                    fontSize: 40,
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              ),
                              padding: EdgeInsets.all(25),
                            ),
                            Padding(
                              child: Text(
                                  "Listen ALL the instructions, memorize, reproduce. If done correctly you'll level up and next round will have one additional instruction. First round is one instruction, good luck!",
                                  style:
                                  TextStyle(color: elementColor, fontSize: 20),
                                  textAlign: TextAlign.center),
                              padding: EdgeInsets.all(25),
                            ),
                            Padding(
                              child: RaisedButton(
                                child: Text(
                                  'Got it!',
                                  style: TextStyle(color: elementColor),
                                ),
                                color: buttonColor,
                                onPressed: changeShowTutorial,
                              ),
                              padding: EdgeInsets.all(25),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                  break;
                default:
              }
              return page;
            },
            itemCount: 3,
            pagination: SwiperPagination(),
            control: SwiperControl(),
          ),
        ),
      ));
    }

    return Scaffold(
      backgroundColor: backgroundColor,
        body: SingleChildScrollView(
          child: Container(

      child: Center(
        child: Column(children: <Widget>[
          Padding(
              padding: EdgeInsets.only(
                top: 60,
              ),
              child: Semantics(
                  label: "username",
                  child: Text('$_username',
                      style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.w200,
                          color: elementColor)))),
          Padding(
              padding: EdgeInsets.only(
                top: 100,
              ),
              child: Text('Highscore',
                  style: TextStyle(fontSize: 30, color: elementColor))),
          Padding(
              padding: EdgeInsets.only(top: 5),
              child: Text('$_highscore',
                  style: TextStyle(fontSize: 110, color: elementColor))),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Semantics(
                          hint: "play",
                          child: IconButton(
                            icon: Icon(
                              Icons.play_arrow,
                              color: elementColor,
                            ),
                            // color: Colors.green,
                            iconSize: 170,
                            onPressed: () => updateHighscore(),
                          )),
                    ],
                  )),
              Container(
                  width: 120,
                  height: 50,
                  child: MaterialButton(
                    child: Text(
                      'Multiplayer',
                      style: TextStyle(color: backgroundColor),
                    ),
                    color: elementColor,
                    onPressed: () => accessMultiplayer(),
                  )),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Padding(
                    padding: EdgeInsets.only(top: 5),
                    child: Semantics(
                        hint: "ranking",
                        child: IconButton(
                          icon: Icon(Icons.grade, color: elementColor),
                          iconSize: 80,
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MemoryGameRanking()));
                          },
                        ))),
                Padding(
                    padding: EdgeInsets.only(top: 5),
                    child: Semantics(
                        hint: "settings",
                        child: IconButton(
                          icon: Icon(
                            Icons.settings,
                            color: elementColor,
                          ),
                          iconSize: 80,
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        MemoryGameSettings()));
                          },
                        ))),
              ]),
            ],
          )
        ]),
      ),
    )));
  }
}
