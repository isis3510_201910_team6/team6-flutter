import 'package:flutter/material.dart';
import 'package:ping/main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ping/main.dart';

class MemoryGameSettings extends StatefulWidget {
  @override
  _MemoryGameSettingsState createState() => _MemoryGameSettingsState();
}

class _MemoryGameSettingsState extends State<MemoryGameSettings> {
  String _soundType = settings['soundType'];
  int _controls = settings['controls'];
  bool _backgroundTrack = settings['backgroundTrack'];
  bool _positiveFeedback = settings['positiveFeedback'];  // Vibration
  bool _negativeFeedback = settings['negativeFeedback'];  //Vibration
  SharedPreferences sp;

  _MemoryGameSettingsState() {
    // Load settings values
  }

  void changeControls(int value) {
    settings['controls'] = value;
    setState(() => _controls = value);
  }

   void changeSoundType(String value) {
    settings['soundType'] = value;
    setState(() => _soundType = value);
  }

   void changeBackgroundTrack(bool value) {
     settings['backgroundTrack'] = value;
    setState(() => _backgroundTrack = value);
  }

  void changePositiveFeedback(bool value) {
    settings['positiveFeedback'] = value;
    setState(() => _positiveFeedback = value);
  }

  void changeNegativeFeedback(bool value) {
    settings['negativeFeedback'] = value;
    setState(() => _negativeFeedback = value);
  }

  @override
  void dispose() {
    saveSettings();
    super.dispose();
  }

  void saveSettings() async {
    sp = await SharedPreferences.getInstance();
    await sp.setInt('controls', _controls);
    await sp.setString('sound_type', _soundType);
    await sp.setBool('background_track', _backgroundTrack);
    await sp.setBool('positive_feedback', _positiveFeedback);
    await sp.setBool('negative_feedback', _negativeFeedback);
  }

  @override
  Widget build(BuildContext context) {
    const titleStyle = TextStyle(fontSize: 20.0, fontWeight: FontWeight.w500, color: elementColor);
    const optionStyle = TextStyle(color: elementColor);
    return Scaffold(
        // appBar: AppBar(
        //   title: Text('Settings'),
        //   centerTitle: true,
        // ),
        backgroundColor: backgroundColor,
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(child:Text('Controls', style: titleStyle),padding: EdgeInsets.only(top:50),),
              RadioListTile<int>(
                title: Text('4 (swipes)', style: optionStyle),
                value: swipes,
                groupValue: _controls,
                onChanged: changeControls,
                activeColor: elementColor,
              ),
              RadioListTile<int>(
                title: Text('5 (swipes + tap)', style: optionStyle),
                value: swipesTap,
                groupValue: _controls,
                onChanged: changeControls,
                activeColor: elementColor,
              ),
              RadioListTile<int>(
                title: Text('6 (swipes + tap + shake)', style: optionStyle),
                value: swipesTapShake,
                groupValue: _controls,
                onChanged: changeControls,
                activeColor: elementColor,
              ),
              Text('Sound Type', style: titleStyle),
              RadioListTile<String>(
                title: Text('Default', style: optionStyle),
                value: soundDefault,
                groupValue: _soundType,
                onChanged: changeSoundType,
                activeColor: elementColor,
              ),
              RadioListTile<String>(
                title: Text('Conga', style: optionStyle),
                value: soundConga,
                groupValue: _soundType,
                onChanged: changeSoundType,
                activeColor: elementColor,
              ),
              RadioListTile<String>(
                title: Text('Piano', style: optionStyle),
                value: soundPiano,
                groupValue: _soundType,
                onChanged: changeSoundType,
                activeColor: elementColor,
              ),
              RadioListTile<String>(
                title: Text('None', style: optionStyle),
                value: soundNone,
                groupValue: _soundType,
                onChanged: changeSoundType,
                activeColor: elementColor,
              ),
              CheckboxListTile(
                title: Text('Background track', style: optionStyle),
                value: _backgroundTrack,
                onChanged: changeBackgroundTrack,
                activeColor: elementColor,
              ),
              CheckboxListTile(
                title: Text('Positive feedback sound', style: optionStyle),
                value: _positiveFeedback,
                onChanged: changePositiveFeedback,
                activeColor: elementColor,
              ),
              CheckboxListTile(
                title: Text('Negative feedback vibration', style: optionStyle),
                value: _negativeFeedback,
                onChanged: changeNegativeFeedback,
                activeColor: elementColor,
              ),
            ],
          ),
        ));
  }
}
