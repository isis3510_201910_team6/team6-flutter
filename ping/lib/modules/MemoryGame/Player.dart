class Player {
  String username;
  int score;
  bool isDead;

  Player(String pUsername) {
    username = pUsername;
    score = 0;
    isDead = false;
  }
}
