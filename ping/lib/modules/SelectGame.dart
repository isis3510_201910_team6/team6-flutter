import 'package:flutter/material.dart';
import './MemoryGame/MemoryGameMenu.dart';


// Deprecated class
class SelectGame extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        Padding(
            padding: EdgeInsets.only(top: 50, bottom: 30),
            child: Center(
                child: Text(
              'Select a game',
              style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
            ))),
        Padding(
          padding:EdgeInsets.only(top:10,bottom: 10),
          child:ButtonTheme(
          minWidth: 200,
          height: 60,
          child:RaisedButton(
          color: Colors.black,
          textColor: Colors.white,
          child: Text('Memory Game',style:TextStyle(fontSize: 20)),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => MemoryGameMenu()));
          },
        ))),
        Padding(
          padding:EdgeInsets.only(top:10,bottom: 10),child:ButtonTheme(
          minWidth: 200,
          height: 60,
          child:RaisedButton(
          onPressed: null,
          color: Colors.black,
          textColor: Colors.white,
          child: Text('Laberynth',style:TextStyle(fontSize: 20)),

          // onPressed: () {
          //   Navigator.push(context,
          //       MaterialPageRoute(builder: (context) => MemoryGameMenu()));
          // },
        ))),
        Padding(
          padding:EdgeInsets.only(top:10,bottom: 10),child:ButtonTheme(
          minWidth: 200,
          height: 60,
          child:RaisedButton(
          onPressed: null,
          color: Colors.black,
          textColor: Colors.white,
          child: Text('More...',style:TextStyle(fontSize: 20)),

          // onPressed: () {
          //   Navigator.push(context,
          //       MaterialPageRoute(builder: (context) => MemoryGameMenu()));
          // },
        ))),
      ],
    ));
  }
}
