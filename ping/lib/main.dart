import 'package:flutter/material.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
// import './modules/SelectGame.dart';
import './modules/RegisterUser.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  // Set `enableInDevMode` to true to see reports while in debug mode
  // This is only to be used for confirming that reports are being
  // submitted as expected. It is not intended to be used for everyday
  // development.
  // Crashlytics.instance.enableInDevMode = true;

  // Pass all uncaught errors to Crashlytics.
  FlutterError.onError = (FlutterErrorDetails details) {
    Crashlytics.instance.onError(details);
  };
  // Test crash
  // Crashlytics.instance.crash();
  runApp(MyApp());
} 

final analytics = FirebaseAnalytics();
final observer = FirebaseAnalyticsObserver(analytics: analytics);

// Colors
const Color backgroundColor = Color(0xFF1C1F33);
const Color elementColor = Color(0xFFCA9995);
const Color buttonColor = Color(0x26000000);
Map<String, dynamic> settings;

// Settings options
// Controls
const int swipes = 4;
const int swipesTap = 5;
const int swipesTapShake = 6;
//Sound type
const String soundDefault = 'default' ;
const String soundConga = 'conga';
const String soundPiano = 'piano';
const String soundNone = 'none';

void loadSettings() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    int controls = sp.getInt('controls');
    String soundType = sp.getString('sound_type');
    bool backgroundTrack = sp.getBool('background_track');
    bool positiveFeedback = sp.getBool('positive_feedback');
    bool negativeFeedback = sp.getBool('negative_feedback');
    
    settings = {
      'controls' : controls ?? swipes,
      'soundType' : soundType ?? soundDefault,
      'backgroundTrack' : backgroundTrack ?? false,
      'positiveFeedback' : positiveFeedback ?? true,
      'negativeFeedback' : negativeFeedback ?? true,
    };
  }

class MyApp extends StatelessWidget {

  MyApp() {
    analytics.logAppOpen();
    loadSettings();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner:false,
      title: 'Ping',
      theme: ThemeData(
        // primarySwatch: Colors.blue,
        primaryColor: Color(0xFFCA9995),
        backgroundColor: Color(0xFF1C1F33)
      ),
      // home: SelectGame(),
      home: RegisterUser(),
      navigatorObservers: [observer],
    );
  }
}


